﻿
using Add2Num;
using System;

class Program
{
    static void Main(string[] args)
    {
        MyBigNumber myBigNumber = new MyBigNumber();

        Console.WriteLine("Enter the first number:");
        string stn1 = Console.ReadLine();

        Console.WriteLine("Enter the second number:");
        string stn2 = Console.ReadLine();

        string result = myBigNumber.Sum(stn1, stn2);
        Console.WriteLine("Final Result: " + result);
    }
}