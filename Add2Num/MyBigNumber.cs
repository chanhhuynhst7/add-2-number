﻿using System.Text;

namespace Add2Num
{
    public class MyBigNumber
    {
        public string Sum(string stn1, string stn2)
        {
            int len1 = stn1.Length; //length of string 1
            int len2 = stn2.Length; //length of string 2
            int maxLen = Math.Max(len1, len2); // maximum length of 2 string
            int num1, num2, sum, count = 0; //num1 and num2 save the value of corresponding digits; sum saves the sum of two digits and count saves the memory value.
            StringBuilder result = new StringBuilder("");

            for (int i = 0; i < maxLen; i++)
            {
                if(i < len1)
                {
                    num1 = stn1[len1 - i - 1] - '0'; // Get the digit from the string 1                                            
                }
                else
                {
                    num1 = 0; //At the end of the string get the value 0
                }
                if(i< len2)
                {
                    num2 = stn2[len2 - i - 1] - '0'; // Get the digit from the string 2                                                  
                }
                else
                {
                    num2 = 0; //At the end of the string get the value 0
                }

                sum = num1 + num2 + count; 
                Console.Write("this is total of adding two separate digits:");
                Console.WriteLine(sum);
               
                count = sum / 10;
                Console.Write("this is count:");
                Console.WriteLine(count);
               
                sum = sum % 10;
                Console.Write("this is result after adding two separate digits:");
                Console.WriteLine(sum);
                Console.WriteLine();

                result.Insert(0, sum);             
            }

            if (count > 0)
            {
                result.Insert(0, count); //At the end of the string adding count to the result 
            }

            return result.ToString();
        }
    }
}
